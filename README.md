# Сервис уведомлений

#### Сервис управления рассылками API администрирования и получения статистики.

## Описание

* Реализованы методы создания новой рассылки, просмотра созданных и получения статистики по выполненным рассылкам.

* Реализован сервис отправки уведомлений на внешнее API.

_В качестве внешнего API был реализован сервис (**FakeAPI**), написанный на FastAPI_

---
# Техническое задание
___


* После создания новой рассылки, если текущее время больше времени начала и меньше времени окончания - должны быть выбраны из справочника все клиенты, которые подходят под значения фильтра, указанного в этой рассылке и запущена отправка для всех этих клиентов.

* Если создаётся рассылка с временем старта в будущем - отправка должна стартовать автоматически по наступлению этого времени без дополнительных действий со стороны пользователя системы.

* По ходу отправки сообщений должна собираться статистика (см. описание сущности "сообщение" выше) по каждому сообщению для последующего формирования отчётов.

* Внешний сервис, который принимает отправляемые сообщения, может долго обрабатывать запрос, отвечать некорректными данными, на какое-то время вообще не принимать запросы. Необходимо реализовать корректную обработку подобных ошибок. Проблемы с внешним сервисом не должны влиять на стабильность работы разрабатываемого сервиса рассылок.

## Дополнительное задание

1. Организовать тестирование написанного кода
2. Обеспечить автоматическую тестирование с помощью GitLab CI
3. Подготовить docker-compose для запуска всех сервисов проекта одной командой
4. Документация со Swagger UI по адресу _/docs/_
5. Удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
6. Обеспечить логирование

___
### Используемые технологии
___
* Python 3.8
* DRF
* FastAPI
* RabbitMQ
* Celery
* PostgreSQL
* Docker, docker-compose
* Gitlab CI/CD