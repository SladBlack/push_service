import datetime
import pytz

from push_service.tasks import send
from mailing.services.messages import create_message
from mailing.models import Client


def send_mailing(sender, instance, **kwargs):
    instance_time = instance.start_timestamp
    instance_end_time = instance.end_timestamp
    current_time = datetime.datetime.now().replace(tzinfo=pytz.utc)
    exp_time = int((instance_end_time - current_time).total_seconds()) * 1000
    clients = Client.objects.filter(**instance.properties).only('phone_number')

    for client in clients:
        msg = create_message(mailing_id=instance.id,
                             client_id=client.id)

        if instance_time <= current_time:
            send(phone_number=client.phone_number,
                 message=instance.message,
                 msg_id=msg.id,
                 exp_time=exp_time)
        else:
            send.apply_async((client.phone_number,
                              instance.message,
                              msg.id,
                              exp_time),
                             eta=instance_time)
