from django.urls import path

from . import views

urlpatterns = [
    path('mailing/', views.MailingListCreateAPIView.as_view(), name='mailing'),
    path('mailing/<int:pk>/', views.MailingRetrieveUpdateDestroyAPIView.as_view(), name='mailing-detail'),
    path('clients/', views.ClientListCreateAPIView.as_view()),
    path('clients/<int:pk>/', views.ClientRetrieveUpdateDestroyAPIView.as_view()),
    path('stat/', views.StatListAPIView.as_view()),
    path('stat/<int:pk>/', views.DetailStatAPIView.as_view()),
]
