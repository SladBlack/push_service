from rest_framework import serializers
from django.db import transaction
from django.db.models.signals import post_save

from . import models
from .signals import send_mailing


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Mailing
        fields = '__all__'

    @transaction.atomic
    def create(self, validated_data):
        post_save.connect(send_mailing, sender=models.Mailing)
        return models.Mailing.objects.create(**validated_data)


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = '__all__'


class StatSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='messages__status')
    count = serializers.IntegerField()

    class Meta:
        model = models.Mailing
        fields = ('status', 'count')


class MessageSerializer(serializers.ModelSerializer):
    client = ClientSerializer()

    class Meta:
        model = models.Message
        exclude = ('mailing', )


class DetailStatSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True)

    class Meta:
        model = models.Mailing
        fields = ('id',
                  'start_timestamp',
                  'message',
                  'end_timestamp',
                  'properties',
                  'messages')
