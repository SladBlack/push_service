import json
import logging

import pika
from django.core.management.base import BaseCommand

from mailing.services.messages import update_message
from mailing.models import Message
from push_service import settings

logging.basicConfig(handlers=[
    logging.FileHandler("mailing/logs/consumer.log"),
    logging.StreamHandler()
],
    level=logging.INFO,
    format="%(asctime)s - [%(levelname)s] - %(message)s")


class Command(BaseCommand):
    def handle(self, *args, **options):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(settings.RABBIT_HOST))
        channel = connection.channel()
        channel.basic_consume(queue=settings.CONSUME_QUEUE, on_message_callback=self.callback, auto_ack=True)
        logging.info("Started Consuming...")
        channel.start_consuming()

    def callback(self, ch, method, properties, body):
        data = json.loads(body)

        if 'msg_id' in data:
            msg_id = data['msg_id']

            try:
                update_message(msg_id=msg_id, status=Message.SN)
            except ValueError:
                logging.info(f"- Message with id = {msg_id} does not exist")
            else:
                logging.info(f"+ Status of message with id = {msg_id} updated")

        else:
            logging.info(f"- Got the wrong response:")
            logging.info(data)
