import json
import logging

import pika

from django.core.management.base import BaseCommand

from mailing.services.messages import update_message
from mailing.models import Message
from push_service import settings

logging.basicConfig(handlers=[
    logging.FileHandler("mailing/logs/dlx-consumer.log"),
    logging.StreamHandler()
],
    level=logging.INFO,
    format="%(asctime)s - [%(levelname)s] - %(message)s")


class Command(BaseCommand):
    def handle(self, *args, **options):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(settings.RABBIT_HOST))
        channel = connection.channel()

        channel.exchange_declare(exchange=settings.DEAD_LETTER_EXCHANGE, durable=True)
        channel.queue_declare(queue=settings.DEAD_LETTER_QUEUE)

        channel.queue_bind(exchange=settings.DEAD_LETTER_EXCHANGE,
                           routing_key=settings.DEAD_LETTER_ROUTING,
                           queue=settings.DEAD_LETTER_QUEUE)

        channel.basic_consume(queue=settings.DEAD_LETTER_QUEUE,
                              on_message_callback=self.callback,
                              auto_ack=True)

        logging.info("Started dlx Consuming...")
        channel.start_consuming()

    def callback(self, ch, method, properties, body):
        data = json.loads(body)

        if 'msg_id' in data:
            msg_id = data['msg_id']

            try:
                update_message(msg_id, Message.FL)
                logging.info(f'Message with id = {msg_id} set failed')
            except ValueError:
                logging.info(f'Message with id = {msg_id} doesnt exist')
        else:
            logging.info(f"- Got the wrong message")
            logging.info(data)
