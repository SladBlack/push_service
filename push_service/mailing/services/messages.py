from ..models import Message


def create_message(mailing_id: int, client_id: int):
    return Message.objects.create(mailing_id=mailing_id,
                                  client_id=client_id)


def update_message(msg_id: int, status) -> None:
    if msg := Message.objects.filter(id=msg_id).first():
        msg.status = status
        msg.save()
    else:
        raise ValueError
