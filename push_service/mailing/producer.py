import json

import pika
from push_service import settings

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=settings.RABBIT_HOST, heartbeat=600, blocked_connection_timeout=300))
channel = connection.channel()


def publish(body):
    channel.basic_publish(exchange='',
                          routing_key=settings.PUBLISH_QUEUE,
                          properties=pika.BasicProperties(
                              expiration=str(body['expiration'])
                          ),
                          body=json.dumps(body))
