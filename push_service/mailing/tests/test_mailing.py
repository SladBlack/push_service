import datetime
import json

import pytz
from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse

from mailing.models import Mailing, Client


class ContactTests(APITestCase):
    timestamp = datetime.datetime.now().replace(tzinfo=pytz.utc)

    def setUp(self):
        data1 = {'tag': 'd1', 'operator_code': '900'}
        data2 = {'tag': 'd2', 'operator_code': '901'}

        mailing_list = list()
        clients_list = list()

        mailing_list.append(Mailing(
            start_timestamp=self.timestamp,
            message='Test Message 1',
            end_timestamp=self.timestamp + datetime.timedelta(hours=1),
            properties=json.dumps(data1)
        ))
        mailing_list.append(Mailing(
            start_timestamp=self.timestamp,
            message='Test Message 2',
            end_timestamp=self.timestamp + datetime.timedelta(hours=2),
            properties=json.dumps(data2)
        ))

        clients_list.append(
            Client(
                phone_number='79021239134',
                timezone='Europe/Moscow',
                operator_code='902',
                tag='first_tag'
            )
        )

        Mailing.objects.bulk_create(mailing_list)
        Client.objects.bulk_create(clients_list)

    def test_list_mailing(self):
        url = reverse('mailing')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_create_mailing(self):
        url = reverse('mailing')

        data = {
            "start_timestamp": self.timestamp,
            "message": "test message",
            "end_timestamp": self.timestamp + datetime.timedelta(minutes=10),
            "properties": {
                "tag": "first_tag",
                "operator_code": "902"
            }
        }

        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Mailing.objects.count(), 3)

    def test_create_fail_mailing(self):
        url = reverse('mailing')

        data = {
            "start_timestamp": 123,
            "message": 123,
            "end_timestamp": self.timestamp + datetime.timedelta(minutes=10),
            "properties": {
                "tag": "first_tag",
                "operator_code": "902"
            }
        }

        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_mailing(self):
        msg = Mailing.objects.values('id').first()
        url = reverse('mailing-detail', kwargs={'pk': msg['id']})

        expected_data = {
            "id": msg['id'],
            "start_timestamp": self.timestamp.replace(tzinfo=None).isoformat() + 'Z',
            "message": 'Test Message 1',
            "end_timestamp": (self.timestamp + datetime.timedelta(hours=1)).replace(tzinfo=None).isoformat() + 'Z',
            "properties": '{"tag": "d1", "operator_code": "900"}'
        }

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, expected_data)

    def test_update_mailing(self):
        msg = Mailing.objects.values('id').last()
        url = reverse('mailing-detail', kwargs={'pk': msg['id']})

        expected_data = {
            "id": msg['id'],
            "start_timestamp": self.timestamp.replace(tzinfo=None).isoformat() + 'Z',
            "message": 'New Test Message 2',
            "end_timestamp": (self.timestamp + datetime.timedelta(hours=5)).replace(tzinfo=None).isoformat() + 'Z',
            "properties": {"tag": "dd1", "operator_code": "910"}
        }

        new_data = {
            "id": msg['id'],
            "start_timestamp": self.timestamp.replace(tzinfo=None).isoformat() + 'Z',
            "message": 'New Test Message 2',
            "end_timestamp": (self.timestamp + datetime.timedelta(hours=5)).replace(
                tzinfo=None).isoformat() + 'Z',
            "properties": {"tag": "dd1", "operator_code": "910"}
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, expected_data)

