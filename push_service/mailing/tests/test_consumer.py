import datetime
import json

import pytz
from rest_framework.test import APITestCase
from django.urls import reverse

from mailing.models import Message, Client
from mailing.management.commands.consumer import Command as Consumer
from mailing.management.commands.dlx_consumer import Command as ConsumerDLX


class ConsumerTests(APITestCase):
    timestamp = datetime.datetime.now().replace(tzinfo=pytz.utc)

    def setUp(self):
        url = reverse('mailing')

        Client.objects.create(
            phone_number='79021239134',
            timezone='Europe/Moscow',
            operator_code='902',
            tag='first_tag'
        )

        data = {
            'start_timestamp': self.timestamp,
            'message': 'Test Message 1',
            'end_timestamp': self.timestamp + datetime.timedelta(hours=1),
            "properties": {
                "tag": "first_tag",
                "operator_code": "902"
            }
        }

        response = self.client.post(url, data=data, format='json')

        self.mailing_id = response.data['id']

    def test_consume_message(self) -> None:
        msg = Message.objects.get(mailing_id=self.mailing_id)
        self.assertEqual(msg.status, Message.PN)

        body = {'msg_id': msg.id}
        cmd = Consumer()
        cmd.callback(ch=None, method=None, properties=None,
                     body=json.dumps(body))
        msg = Message.objects.get(id=msg.id)

        self.assertEqual(msg.status, Message.SN)

    def test_consume_incorrect_message(self):
        body = {'ns_id': '123'}
        cmd = Consumer()
        cmd.callback(ch=None, method=None, properties=None,
                     body=json.dumps(body))

    def test_dlx_consumer(self):
        msg = Message.objects.get(mailing_id=self.mailing_id)
        self.assertEqual(msg.status, Message.PN)

        body = {'msg_id': msg.id}
        cmd = ConsumerDLX()
        cmd.callback(ch=None, method=None, properties=None,
                     body=json.dumps(body))

        msg = Message.objects.get(id=msg.id)
        self.assertEqual(msg.status, Message.FL)
