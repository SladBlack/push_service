from django.db.models import Count
from rest_framework import generics
from rest_framework.permissions import AllowAny

from . import models
from . import serializers


class MailingListCreateAPIView(generics.ListCreateAPIView):
    queryset = models.Mailing.objects.all()
    serializer_class = serializers.MailingSerializer
    permission_classes = [AllowAny]


class MailingRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.MailingSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return models.Mailing.objects.filter(id=self.kwargs['pk'])


class ClientListCreateAPIView(generics.ListCreateAPIView):
    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer
    permission_classes = [AllowAny]


class ClientRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.ClientSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return models.Client.objects.filter(id=self.kwargs['pk'])


class StatListAPIView(generics.ListAPIView):
    serializer_class = serializers.StatSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return models.Mailing.objects.values(
            'messages__status'
        ).annotate(count=Count('messages__status'))


class DetailStatAPIView(generics.RetrieveAPIView):
    serializer_class = serializers.DetailStatSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return models.Mailing.objects.filter(pk=self.kwargs['pk'])
