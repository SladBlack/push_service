import pytz

from django.db import models
from django.core.validators import RegexValidator


class Mailing(models.Model):
    start_timestamp = models.DateTimeField()
    message = models.TextField()
    end_timestamp = models.DateTimeField()
    properties = models.JSONField()

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(validators=[RegexValidator(r"7\d{10}$")], max_length=11, unique=True)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')
    operator_code = models.CharField(max_length=3, validators=[RegexValidator(r'^\d{0,3}$')])
    tag = models.CharField(max_length=20)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    SN = 'SENT'
    PN = 'PENDING'
    FL = 'FAILED'

    STATUS = [
        (PN, 'Pending'),
        (SN, 'Message was sent'),
        (FL, 'Failed'),
    ]

    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=STATUS, max_length=15, default=PN)
    mailing = models.ForeignKey('Mailing', on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey('Client', on_delete=models.SET_NULL, null=True, related_name='messages')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
