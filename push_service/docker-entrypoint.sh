#!/bin/bash

if [ "$?" == "0" ]; then
	python manage.py makemigrations
	python manage.py migrate
fi

exec "$@"