from celery import shared_task

from mailing.producer import publish


@shared_task
def send(phone_number: str, message: str, msg_id: int, exp_time: int) -> None:
    """Send messages to FakeAPI"""
    publish({'phone': phone_number,
             'message': message,
             'msg_id': msg_id,
             'expiration': exp_time})
