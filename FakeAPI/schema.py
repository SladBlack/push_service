from pydantic import BaseModel


class MessageSchema(BaseModel):
    phone: str
    text: str
