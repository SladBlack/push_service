from pydantic import BaseSettings
from decouple import config


class Settings(BaseSettings):
    PUBLISH_QUEUE: str = 'response'
    CONSUME_QUEUE: str = 'message'
    RABBIT_HOST: str = config('RABBIT_HOST', default='rabbitmq')
    RABBIT_PORT: str = config('RABBIT_PORT', default='5672')
    DEAD_LETTER_EXCHANGE: str = 'dlx'
    DEAD_LETTER_QUEUE: str = 'dl'
    DEAD_LETTER_ROUTING: str = 'dl'


settings = Settings()
