import json
import uuid
import logging
import random

import pika
from settings import settings
from aio_pika import connect_robust

logging.basicConfig(handlers=[
    logging.FileHandler("logs/fakeapi.log"),
    logging.StreamHandler()
],
    level=logging.INFO,
    format="%(asctime)s - [%(levelname)s] - %(message)s")


class PikaClient:

    def __init__(self):
        self.publish_queue_name = settings.PUBLISH_QUEUE
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=settings.RABBIT_HOST)
        )
        self.channel = self.connection.channel()
        self.publish_queue = self.channel.queue_declare(queue=self.publish_queue_name)
        self.callback_queue = self.publish_queue.method.queue
        self.process_callable = self.send_response
        logging.info('Pika connection initialized')

    async def consume(self, loop):
        """Setup message listener with the current running loop"""
        connection = await connect_robust(host=settings.RABBIT_HOST,
                                          port=settings.RABBIT_PORT,
                                          loop=loop)
        channel = await connection.channel()

        queue = await channel.declare_queue(
            settings.CONSUME_QUEUE,
            arguments={
                'x-dead-letter-exchange': settings.DEAD_LETTER_EXCHANGE,
                'x-dead-letter-routing-key': settings.DEAD_LETTER_ROUTING,
            })

        await queue.consume(self.process_incoming_message, no_ack=False)
        logging.info('Established pika async listener')
        return connection

    async def process_incoming_message(self, message):
        """Processing incoming message from RabbitMQ"""
        message.ack()
        data = json.loads(message.body)

        logging.info('Received message')
        logging.info(data)

        methods = [self.send_response, self.send_incorrect_response]
        sending_method = methods[random.randint(0, len(methods) - 1)]
        sending_method(data)

    def send_response(self, message: dict):
        """Method to publish message to RabbitMQ"""
        logging.info('Sending response to PushService')
        self.channel.basic_publish(
            exchange='',
            routing_key=self.publish_queue_name,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=str(uuid.uuid4()),
            ),
            body=json.dumps(message)
        )

    def send_incorrect_response(self, message: dict):
        """Send response with incorrect data"""
        incorrect_data = {'ms_id': 1, 'status': 'bad'}
        logging.info(f'-Service sent incorrect response')
        self.send_response(incorrect_data)
