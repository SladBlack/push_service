from fastapi import Request, APIRouter

from schema import MessageSchema

router = APIRouter()


@router.post('/send-message')
async def send_message(payload: MessageSchema, request: Request):
    request.app.pika_client.send_response(
        {"phone": payload.phone,
         "text": payload.text}
    )
    return {"code": 1,
            "message": "message was sent success"}
